# Noote

This's a siple note web app create with **Sveltekit**.

The result:

![Noote app](static/noote.gif)

## Get the noote

Run this command to download this project:

```bash
git clone https://gitlab.com/mr-fox/noote.git
```

## Run the project

Once you've download this project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```
