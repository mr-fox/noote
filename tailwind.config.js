/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        primary: {
          20: "var(--primary-20)",
          40: "var(--primary-40)",
          60: "var(--primary-60)",
          80: "var(--primary-80)",
          100: "var(--primary-100)",
        },
        black: "#0b132b",
        white: "#f7f7ff"
      }
    },
    fontFamily: {
      "jetbrains-mono": "JetBrains Mono",
    }
  },
  plugins: [],
}